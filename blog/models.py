from django.db import models
from django.utils import timezone

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length = 200)
    text = models.TextField()
    likes = models.IntegerField()
    created_date = models.DateTimeField(default=timezone.now)
    comments = models
    def __str__(self):
        return self.title

class Comment(models.Model):
    text = models.TextField()
    created_date = models.DateTimeField(default = timezone.now)
    blog_id = models.IntegerField()
    def __str__(self):
        return self.created_date