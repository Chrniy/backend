from django.shortcuts import render, get_object_or_404, redirect
from blog.models import Post, Comment
from .forms import PostForm, CommentForm

# Create your views here.
def post_list(request):
    return render(request, 'blog/post_list.html' , context = {"posts" : reversed(Post.objects.all())})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk = pk)
    return render(request, 'blog/post_detail.html', context={'post': post, "comments" : reversed(Comment.objects.filter(blog_id = post.id).all())})

def post_new(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.likes = 0
            post.save()
            return redirect('post_list')
    form = PostForm()
    return render(request, 'blog/post_new.html', {'form': form})

def post_like (request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.likes += 1
    post.save()
    return redirect('post_list')

def post_delete (request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return redirect('post_list')

def post_main (request):
    return redirect('post_list')

def add_comment (request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.blog_id = post.id
            comment.save()
            return redirect('post_detail', pk = post.pk)
    form = CommentForm()
    return render(request, 'blog/new_comment.html', {'form': form})







