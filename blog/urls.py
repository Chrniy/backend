from django.urls import path
from blog import views

urlpatterns = [
    path('', views.post_list, name = 'post_list'),
    path('post/<int:pk>/', views.post_detail, name = 'post_detail'),
    path('post_new', views.post_new, name = 'post_new'),
    path('post_like/<int:pk>/', views.post_like, name = 'post_like'),
    path('post_main', views.post_main, name = 'post_main'),
    path('post_delete/<int:pk>/', views.post_delete, name='post_delete'),
    path('add_comment/<int:pk>/', views.add_comment, name='add_comment'),
]